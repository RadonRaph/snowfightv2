using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarThenDisable : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(WaitEof());
    }

    IEnumerator WaitEof()
    {
        yield return new WaitForEndOfFrame();
        gameObject.SetActive(false);
    }
}