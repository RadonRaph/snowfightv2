using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

public class InZoneDrawShadow : MonoBehaviour
{

    private HDAdditionalLightData light;

    private bool _inZone;

    private void Start()
    {
        light = GetComponent<HDAdditionalLightData>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_inZone)
        {
            light.RequestShadowMapRendering();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (LayerMask.GetMask("Player") == other.gameObject.layer)
        {
            _inZone = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (LayerMask.GetMask("Player") == other.gameObject.layer)
        {
            _inZone = false;
        }
    }
}
