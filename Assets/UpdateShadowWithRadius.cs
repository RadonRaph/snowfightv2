using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

public class UpdateShadowWithRadius : MonoBehaviour
{
    private HDAdditionalLightData _lightData;

    public float radius;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, radius);
    }

    // Start is called before the first frame update
    void Start()
    {
        _lightData = GetComponent<HDAdditionalLightData>();
    }

    // Update is called once per frame
    void Update()
    {

        foreach (Camera cam in Camera.allCameras)
        {
            Debug.Log(cam);
            if (Vector3.Distance(transform.position, cam.transform.position)<radius)
            {
                Debug.Log("Update Shadow " + gameObject.name, gameObject);
                _lightData.RequestShadowMapRendering();
                return;
            }
        }
        
    }
}
