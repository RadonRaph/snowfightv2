using System.Collections.Generic;
using System.Linq;
using Player;
using UnityEngine;

namespace Network {
    public static class PlayersManager {
        private static Dictionary<ulong, PersistentPlayer> players = new Dictionary<ulong, PersistentPlayer>();
        public static PersistentPlayer LocalPlayer { get; private set; }
        
        public static void Register(ulong networkId, PersistentPlayer player, bool isLocal = false) {
            players.Add(networkId, player);
            player.gameObject.name = "Player #" + networkId;
            if (isLocal) {
                LocalPlayer = player;
            }
        }

        public static void Unregister(ulong networkId) {
            players.Remove(networkId);
        }

        public static PersistentPlayer GetPlayerById(ulong networkId) {
            return players[networkId];
        }

        public static PersistentPlayer GetPlayerByName(string playerName) {
            string lowerPlayerName = playerName.ToLower();
            return players.Where(pair => {
                    string otherPlayerName = pair.Value.PlayerName.Value;
                    return otherPlayerName != null && otherPlayerName.ToLower().Equals(lowerPlayerName);
                })
                .Select(pair => pair.Value)
                .FirstOrDefault();
        }

        public static Dictionary<ulong, PersistentPlayer>.ValueCollection GetAllPlayers() {
            return players.Values;
        }

    }
}