using UnityEngine;
using UnityEngine.VFX;

public class SnowmenColorManager : MonoBehaviour {
    public MeshRenderer ScarfRenderer;
    public VisualEffect Eye1;
    public VisualEffect Eye2;
    public Light light1;
    public Light light2;

    public void ChangeColor(Color color) {
        ScarfRenderer.material.SetColor("_Tint", color);
        Eye1.SetVector4("Color", color);
        Eye2.SetVector4("Color", color);
        light1.color = color;
        light2.color = color;
    }
}