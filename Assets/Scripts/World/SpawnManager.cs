using System.Collections.Generic;
using Player;
using Unity.Netcode;
using UnityEngine;
using Random = System.Random;

namespace World {
    public class SpawnManager : MonoBehaviour {

        public static SpawnManager Instance { get; private set; }

        // Variables
        private readonly List<SpawnPoint> spawns = new List<SpawnPoint>();
        private readonly Collider[] hitCacheColliders = new Collider[1];

        [SerializeField] private LayerMask _playersMask;

        private void Awake() {
            if (Instance == null) {
                Instance = this;
            } else {
                Destroy(this);
                return;
            }

            foreach (Transform child in transform) {
                SpawnPoint spawnPoint = child.GetComponent<SpawnPoint>();
                if (spawnPoint != null) {
                    spawns.Add(spawnPoint);
                } else {
                    Debug.LogError("A child of the SpawnManager does not have the SpawnPoint component.");
                }
            }
        }

        public SpawnPoint GetFreeSpawn() {
            List<SpawnPoint> availableSpawns = spawns.FindAll(IsFreeSpawn);
            return availableSpawns.Count > 0
                ? availableSpawns[new Random().Next(0, availableSpawns.Count)]
                : spawns[new Random().Next(0, spawns.Count)];
        }

        private bool IsFreeSpawn(SpawnPoint spawn) {
            return Physics.OverlapSphereNonAlloc(spawn.transform.position, 3.0f, hitCacheColliders, _playersMask) == 0;
        }
    }
}