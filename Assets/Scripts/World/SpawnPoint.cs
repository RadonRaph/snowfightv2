using UnityEngine;

namespace World {
    public class SpawnPoint : MonoBehaviour {

        [SerializeField] private Vector3 startDirection;
        public Vector3 StartDirection => startDirection;
        
        
        private void OnDrawGizmos() {
            Vector3 pos = transform.position;
            Vector3 worldDirection = transform.TransformDirection(startDirection).normalized;
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(pos, pos + worldDirection * 10);
        }
    }
}