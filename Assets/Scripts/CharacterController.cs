using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public Vector3 velocity;
    public float speed = 10f;
    public float drag = 0.9f;
    private float colliderRadius;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Physics.Raycast(transform.position, transform.position + (velocity*Time.deltaTime*2), out hit);
        if (hit.transform != null)
        {
            Vector3.ClampMagnitude(velocity, Vector3.Distance(hit.point, transform.position) - colliderRadius);
            transform.position += velocity*Time.deltaTime;
            velocity *= 0.1f;
        }
        else
        {
            transform.position += velocity*Time.deltaTime;
        }

        velocity *= drag;
        velocity += Physics.gravity;


    }
    
    public void GetInputs(Vector3 input)
    {
        velocity += input * speed;
    }
}
