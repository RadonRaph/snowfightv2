using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System.Linq;
using Network;
using Player.Gameplay;
using Unity.Netcode;

public class KillFeed : MonoBehaviour
{

    public static KillFeed Instance { get; private set; }
   
    public GameObject killPrefab;
    public Color selfColor;
    public Color otherColor;
    
    
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    private void OnEnable() {
        NetworkSnowballLauncherState.onKillEvent += OnKill;
    }

    private void OnDisable() {
        NetworkSnowballLauncherState.onKillEvent -= OnKill;
    }

    public void OnKill(ulong attackerId, ulong victimId) {
        ulong clientId = NetworkManager.Singleton.LocalClientId;
        KillFeedAdd(PlayersManager.GetPlayerById(attackerId).PlayerName.Value,
            PlayersManager.GetPlayerById(victimId).PlayerName.Value,
            clientId == attackerId,
            clientId == victimId
        );
    }
    

    public void KillFeedAdd(string killer, string killed, bool selfIsKiller = false, bool selfIsKilled = false)
    {
        GameObject obj = Instantiate(killPrefab, transform);
        
        Text t1 = obj.transform.GetChild(0).GetComponent<Text>();
        Text t2 = obj.transform.GetChild(2).GetComponent<Text>();
        t1.text = killer;
        t2.text = killed;

        if (selfIsKiller)
        {
            t1.color = selfColor;
        }
        else
        {
            t1.color = otherColor;
        }

        if (selfIsKilled)
        {
            t2.color = selfColor;
        }
        else
        {
            t2.color = otherColor;
        }
        
        Destroy(obj, 9);
        RecursiveUIFade(obj.transform, 0, 1.5f, 7);
    }



    void RecursiveUIFade(Transform _transform, float fade, float time, float delay = 0)
    {
        Graphic[] self = _transform.GetComponents<Graphic>();
        Graphic[] uis = GetComponentsInChildren<Graphic>().Concat(self).ToArray();

        for (int i = 0; i < uis.Length; i++)
        {
            uis[i].DOFade(fade, time).SetDelay(delay);
        }
    }
}
