using Network;
using Player.Gameplay;
using TMPro;
using Unity.Netcode;
using UnityEngine;

namespace Ui {
    public class HitBy : MonoBehaviour {

        [SerializeField] private TMP_Text hitByText;

        private void OnEnable() {
            NetworkSnowballLauncherState.onKillEvent += OnKill;
            NetworkLogicController.onRespawn += OnRespawn;
        }

        private void OnDisable() {
            NetworkSnowballLauncherState.onKillEvent -= OnKill;
            NetworkLogicController.onRespawn -= OnRespawn;
        }
        
        public void OnKill(ulong attackerId, ulong victimId) {
            ulong clientId = NetworkManager.Singleton.LocalClientId;
            if (clientId == victimId) {
                hitByText.enabled = true;
                hitByText.text = "Hit by " + PlayersManager.GetPlayerById(attackerId).PlayerName.Value;
            }
        }

        public void OnRespawn(ulong clientId) {
            if (clientId == NetworkManager.Singleton.LocalClientId) {
                hitByText.enabled = false;
            }
        }
    }
}