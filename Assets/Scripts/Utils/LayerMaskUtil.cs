using System.Collections.Generic;
using UnityEngine;

namespace Utils {
    public static class LayerMaskUtil {
        public static bool HasLayer(this LayerMask layerMask, int layer) {
            return layerMask == (layerMask | (1 << layer));
        }

        public static List<int> GetLayers(this LayerMask layerMask) {
            List<int> layers = new List<int>();
            for (int i = 0; i < 32; i++) {
                if (layerMask == (layerMask | (1 << i))) {
                    layers.Add(i);
                }
            }

            return layers;
        }

        public static void SetLayerRecursive(this GameObject obj, int layer) {
            obj.layer = layer;
            foreach (Transform child in obj.transform) {
                child.gameObject.SetLayerRecursive(layer);
            }
        }
    }
}