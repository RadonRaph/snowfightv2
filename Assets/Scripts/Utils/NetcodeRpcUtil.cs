﻿using Unity.Collections;
using Unity.Netcode;

namespace Utils {
    public static class NetcodeRpcUtil {

        public static ClientRpcParams GetClientRpcParams(params ulong[] ids) {
            return new ClientRpcParams {
                Send = new ClientRpcSendParams {
                    TargetClientIds = ids
                }
            };
        }
    }

    public struct NetworkString : INetworkSerializable {

        private FixedString32Bytes str;
        
        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter {
            serializer.SerializeValue(ref str);
        }

        public override string ToString() {
            return str.ToString();
        }

        public static implicit operator string(NetworkString ns) => ns.ToString();

        public static implicit operator NetworkString(string s) =>
            new NetworkString() {str = new FixedString32Bytes(s)};

    }
}