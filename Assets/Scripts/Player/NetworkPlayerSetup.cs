using System;
using Network;
using Unity.Netcode;
using Unity.Netcode.Samples;
using UnityEngine;
using UnityEngine.Networking.Types;
using Utils;

namespace Player {
    [DisallowMultipleComponent]
    public class NetworkPlayerSetup : NetworkBehaviour {
        
        [SerializeField] private Behaviour[] componentsToDisable;
        [SerializeField] private GameObject thirdPersonGraphics;
        [SerializeField] private GameObject firstPersonGraphics;
        [SerializeField] private LayerMask dontDrawLayerMask;

        private Camera mainCamera;

        public override void OnNetworkSpawn() {
            int dontDrawLayer = dontDrawLayerMask.GetLayers().Count > 0 ? dontDrawLayerMask.GetLayers()[0] : -1;
            PlayersManager.GetPlayerById(OwnerClientId).PlayerInstance = gameObject;
            if (IsOwner) {
                // Disable main camera and 3rd person graphics
                mainCamera = Camera.main;
                if (mainCamera != null) {
                    mainCamera.gameObject.SetActive(false);
                }
                thirdPersonGraphics.SetLayerRecursive(dontDrawLayer);
                Cursor.lockState = CursorLockMode.Locked;
            } else {
                // Disabled all components
                foreach (Behaviour behaviour in componentsToDisable) {
                    behaviour.enabled = false;
                }
                // Disable 1st person graphics
                firstPersonGraphics.SetLayerRecursive(dontDrawLayer);
            }
            DontDestroyOnLoad(this);
        }

        public override void OnNetworkDespawn() {
            PlayersManager.Unregister(OwnerClientId);
        }

        private void OnDisable() {
            if (mainCamera != null) {
                mainCamera.gameObject.SetActive(true);
            }
        }
    }
}