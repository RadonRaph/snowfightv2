using System.Linq;
using Network;
using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.VFX;

namespace Player {
    public class PlayerGraphicsController : NetworkBehaviour {

        [SerializeField] private VisualEffect[] burningEyes;
        [SerializeField] private Light[] lights;
        [SerializeField] private Camera myCamera;
        [SerializeField] private Canvas playerNameCanvas;
        [SerializeField] private TMP_Text playerName;
        [Header("Color")] [SerializeField] private SnowmenColorManager[] colorManagers;
        
        public NetworkVariable<float> color = new NetworkVariable<float>();
        private Camera cameraToFace;

        private void Awake() {
            color.OnValueChanged += ChangeColor;
            cameraToFace = null;
            enabled = false;
        }

        public override void OnDestroy() {
            base.OnDestroy();
            color.OnValueChanged -= ChangeColor;
        }

        public override void OnNetworkSpawn() {
            playerName.text = PlayersManager.GetPlayerById(OwnerClientId).PlayerName.Value;
            ChangeColor(color.Value, color.Value);
            if (IsOwner) {
                ActiveBurningEyes(false);
            }
        }

        public void SetCameraToFace(Camera cam) {
            cameraToFace = cam;
            enabled = true;
        }

        private void ChangeColor(float oldColor, float newColor) {
            Color rgbColor = Color.HSVToRGB(newColor, 1, 1);
            foreach (SnowmenColorManager colorManager in colorManagers) {
                colorManager.ChangeColor(rgbColor);
            }
            playerName.color = rgbColor;
        }

        [ClientRpc]
        public void HandleCameraFacingClientRpc() {
            playerName.text = PlayersManager.GetPlayerById(OwnerClientId).PlayerName.Value;
            // On fait en sorte que le nom soit fasse face à la caméra de l'autre joueur
            if (IsOwner) {
                playerNameCanvas.enabled = false;
                foreach (PersistentPlayer persistentPlayer in PlayersManager.GetAllPlayers()
                    .Where(persistentPlayer => persistentPlayer.OwnerClientId != NetworkManager.Singleton.LocalClientId)) {
                    GameObject playerInstance = persistentPlayer.PlayerInstance;
                    if (playerInstance != null) {
                        PlayerGraphicsController graphicsController =
                            playerInstance.GetComponent<PlayerGraphicsController>();
                        graphicsController.cameraToFace = myCamera;
                        graphicsController.enabled = true;
                    }
                }
            } else {
                GameObject playerInstance = PlayersManager.LocalPlayer.PlayerInstance;
                if (playerInstance != null) {
                    PlayerGraphicsController myGraphicsController =
                        playerInstance.GetComponent<PlayerGraphicsController>();
                    cameraToFace = myGraphicsController.myCamera;
                    enabled = true;
                } else {
                    NetworkLog.LogWarningServer("Player instance is null " + NetworkManager.Singleton.LocalClientId);
                }
            }
        }

        public void ActiveBurningEyes(bool active) {
            foreach (VisualEffect eye in burningEyes) {
                VisualEffect burningEffect = eye.GetComponent<VisualEffect>();
                if (burningEffect != null) {
                    if (active) {
                        burningEffect.Play();
                    } else {
                        burningEffect.Stop();
                    }
                }
            }
            foreach (Light l in lights) {
                l.enabled = active;
            }
        }

        private void Update() {
            playerNameCanvas.transform.LookAt(cameraToFace.transform.position, cameraToFace.transform.rotation * Vector3.up);
            playerNameCanvas.transform.Rotate(Vector3.up, 180);
        }
    }
}