using System;
using Player.Gameplay;
using Unity.Netcode;
using UnityEngine;

namespace Player {

    [RequireComponent(typeof(NetworkSnowballLauncherState), typeof(ServerSnowballLauncher))]
    [DisallowMultipleComponent]
    public class ClientActionPerformer : NetworkBehaviour {

        private NetworkSnowballLauncherState snowballLauncherState;
        private ServerSnowballLauncher serverSnowballLauncher;

        private void Awake() {
            snowballLauncherState = GetComponent<NetworkSnowballLauncherState>();
            serverSnowballLauncher = GetComponent<ServerSnowballLauncher>();
        }

        public void PerformLaunchSnowball() {
            serverSnowballLauncher.LaunchSnowballServerRpc();
            if (!IsHost) {
                DoClientPrediction(snowballLauncherState.LaunchSnowball);
            }
        }
        
        private void DoClientPrediction<T>(Func<T> prediction) {
            if (!IsHost) {
                prediction();
            }
        }
    }
}