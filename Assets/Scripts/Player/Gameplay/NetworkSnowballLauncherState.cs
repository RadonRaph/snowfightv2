using System;
using Unity.Netcode;
using UnityEngine;

namespace Player.Gameplay {
    [DisallowMultipleComponent]
    public class NetworkSnowballLauncherState : NetworkBehaviour {
        
        [Header("Snowball launch")]
        [SerializeField] private GameObject snowballSpawn;
        [SerializeField] private Vector3 launchDirection;
        [SerializeField] private float launchForce;
        
        [Header("Prefabs")]
        [SerializeField] private GameObject snowballPrefab;

        public NetworkVariable<bool> canShoot = new NetworkVariable<bool>(true);

        // Events
        public static event Action<ulong, ulong> onKillEvent = delegate { };
        
        public Snowball LaunchSnowball() {
            GameObject snowballObj = Instantiate(snowballPrefab, snowballSpawn.transform.position, Quaternion.identity);
            Snowball snowball = snowballObj.GetComponent<Snowball>();
            if (snowball != null) {
                snowball.Launch(snowballSpawn.transform.TransformDirection(launchDirection).normalized, launchForce, this);
            }
            return snowball;
        }

        [ClientRpc]
        public void OnKillClientRpc(ulong attackerId, ulong victimId) {
            onKillEvent(attackerId, victimId);
        }

        private void OnDrawGizmos() {
            // Draw launch direction
            Gizmos.color = Color.red;
            Vector3 spawn = snowballSpawn.transform.position;
            Vector3 dir = snowballSpawn.transform.TransformDirection(launchDirection);
            Gizmos.DrawLine(spawn, spawn + dir * 10);
        }
    }
}