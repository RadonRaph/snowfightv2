using System;
using Cinemachine;
using Network;
using Unity.Netcode;
using UnityEngine;
using Utils;

namespace Player.Gameplay {
    [RequireComponent(typeof(NetworkPlayerPhysicsController),
        typeof(PlayerGraphicsController))]
    [DisallowMultipleComponent]
    public class NetworkLogicController : NetworkBehaviour {

        // Components
        private NetworkPlayerPhysicsController physicsController;
        private PlayerGraphicsController graphicsController;
        
        [Header("Components")]
        [SerializeField] private CinemachineVirtualCamera virtualDeadCam;
        
        // Layers
        [Header("Layers")]
        [SerializeField] private LayerMask playerLayer;
        [SerializeField] private LayerMask deadLayer;
        
        // Events
        public static Action<ulong> onRespawn = delegate { };

        private void Awake() {
            physicsController = GetComponent<NetworkPlayerPhysicsController>();
            graphicsController = GetComponent<PlayerGraphicsController>();
        }

        public void RespawnServerAndClientSide(Vector3 spawnPos, Vector3 direction) {
            Respawn(spawnPos, direction);
            RespawnClientRpc(spawnPos, direction);
        }
        
        private void Respawn(Vector3 spawnPos, Vector3 direction) {
            physicsController.Teleport(spawnPos, direction);
            physicsController.ResetPhysics();
            if (!IsOwner) {
                graphicsController.ActiveBurningEyes(true);
            }
            gameObject.layer = playerLayer.GetLayers()[0];
            onRespawn(OwnerClientId);
        }

        [ClientRpc]
        private void RespawnClientRpc(Vector3 spawnPos, Vector3 direction) {
            Respawn(spawnPos, direction);
            if (IsOwner) {
                virtualDeadCam.Priority = 5;
                virtualDeadCam.transform.SetParent(gameObject.transform);
                virtualDeadCam.transform.position = gameObject.transform.position + Vector3.up * 2.0f;
                virtualDeadCam.LookAt = null;
            }
        }

        public void DieServerAndClientSide(ulong killerId, Vector3 shootDirection) {
            Die(shootDirection);
            DieClientRpc(killerId, shootDirection);
        }

        private void Die(Vector3 shootDirection) {
            physicsController.Explode(shootDirection + Vector3.up * 0.15f);
            graphicsController.ActiveBurningEyes(false);
            gameObject.layer = deadLayer.GetLayers()[0];
        }
        
        [ClientRpc]
        private void DieClientRpc(ulong killerId, Vector3 shootDirection) {
            Die(shootDirection);
            if (IsOwner) {
                virtualDeadCam.Priority = 20;
                virtualDeadCam.transform.SetParent(null);
                virtualDeadCam.LookAt = PlayersManager.GetPlayerById(killerId).PlayerInstance.transform;

            }
        }
    }
}