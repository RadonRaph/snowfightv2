using System;
using UnityEngine;

namespace Player.Gameplay {
    [RequireComponent(typeof(Rigidbody), typeof(Collider))]
    public class Snowball : MonoBehaviour {

        // Componenents
        private new Rigidbody rigidbody;
        private new Collider collider;
        
        // Variables
        public NetworkSnowballLauncherState Launcher { get; private set; }
        
        private bool launched;
        private float liveDuration;
        
        // Events
        public event Action<GameObject, Snowball> onCollisionEvent = delegate { };

        [Header("Settings")]
        [SerializeField] private float duration;

        private void Awake() {
            rigidbody = GetComponent<Rigidbody>();
            collider = GetComponent<Collider>();
        }

        private void OnEnable() {
            launched = false;
            liveDuration = 0.0f;
        }

        private void Update() {
            if (launched) {
                liveDuration += Time.deltaTime;
                if (liveDuration >= duration) {
                    Destroy(gameObject);
                }
            }
        }

        public void Launch(Vector3 direction, float force, NetworkSnowballLauncherState launcher) {
            rigidbody.AddForce(direction * force, ForceMode.Impulse);
            Launcher = launcher;
            DisableCollideWithLauncher();
            launched = true;
        }

        private void DisableCollideWithLauncher() {
            Collider launcherCollider = Launcher.GetComponent<Collider>();
            if (launcherCollider != null) {
                Physics.IgnoreCollision(launcherCollider, collider);
            }
        }

        private void OnCollisionEnter(Collision other) {
            onCollisionEvent(other.gameObject, this);
            Destroy(gameObject);
            launched = false;
        }
    }
}