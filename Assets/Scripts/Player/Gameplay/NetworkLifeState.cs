using System;
using Unity.Netcode;
using UnityEngine;

namespace Player.Gameplay {
    public class NetworkLifeState : NetworkBehaviour {

        // Network
        public readonly NetworkVariable<bool> alive = new NetworkVariable<bool>(true);
        
        public event Action onDie = delegate { };

        private void OnEnable() {
            alive.OnValueChanged += OnAliveChanged;
        }

        private void OnDisable() {
            alive.OnValueChanged -= OnAliveChanged;
        }

        private void OnAliveChanged(bool wasAlive, bool isAlive) {
            if (wasAlive && !isAlive) {
                onDie();
            }
        }
    }
}