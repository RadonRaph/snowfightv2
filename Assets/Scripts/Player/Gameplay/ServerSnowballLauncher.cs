using System;
using DG.Tweening;
using Unity.Netcode;
using UnityEngine;
using Utils;
using World;

namespace Player.Gameplay {
    
    [RequireComponent(typeof(NetworkSnowballLauncherState))]
    [DisallowMultipleComponent]
    public class ServerSnowballLauncher : NetworkBehaviour {

        private NetworkSnowballLauncherState snowballLauncherState;

        private void Awake() {
            snowballLauncherState = GetComponent<NetworkSnowballLauncherState>();
        }

        [ServerRpc]
        public void LaunchSnowballServerRpc() {
            Snowball snowball = snowballLauncherState.LaunchSnowball(); // Server simulation
            snowball.onCollisionEvent += OnSnowballHit;
            LaunchClientRpc();
        }

        [ClientRpc]
        public void LaunchClientRpc() {
            if (!IsOwner) { // If the snowball has not already been launched
                snowballLauncherState.LaunchSnowball();
            }
        }

        public void OnSnowballHit(GameObject collidedObj, Snowball snowball) {
            snowball.onCollisionEvent -= OnSnowballHit;
            if (!IsServer) {
                return;
            }
            NetworkLifeState playerLife = collidedObj.GetComponent<NetworkLifeState>();
            NetworkLogicController logicController = collidedObj.GetComponent<NetworkLogicController>();
            if (playerLife != null && logicController != null) {
                playerLife.alive.Value = false;
                snowball.Launcher.OnKillClientRpc(snowball.Launcher.OwnerClientId, playerLife.OwnerClientId);
                Vector3 shootDirection = collidedObj.transform.position - snowball.transform.position;
                logicController.DieServerAndClientSide(snowball.Launcher.OwnerClientId, Vector3.Reflect(shootDirection.normalized, Vector3.up).normalized);
                DOVirtual.DelayedCall(4.0f, () => {
                    SpawnPoint spawn = SpawnManager.Instance.GetFreeSpawn();
                    logicController.RespawnServerAndClientSide(spawn.transform.position, spawn.StartDirection);
                    playerLife.alive.Value = true;
                });
            }
        }
    }
}