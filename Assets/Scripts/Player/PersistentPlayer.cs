using System;
using System.Collections.Generic;
using Network;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;
using World;
using Random = UnityEngine.Random;

namespace Player {
    public class PersistentPlayer : NetworkBehaviour {

        [Header("Visuals")]
        [SerializeField] private GameObject playerPrefab;

        public NetworkVariable<NetworkString> PlayerName { get; private set; }
        public GameObject PlayerInstance { get; set; }
        
        // Events
        public static Action<bool> onConnectionResult = delegate { };

        public override void OnNetworkSpawn() {
            PlayersManager.Register(OwnerClientId, this, IsLocalPlayer);
            gameObject.name = "Persistent Player #" + OwnerClientId;
            DontDestroyOnLoad(this);
        }

        [ServerRpc]
        public void TryConnectServerRpc(string playerName) {
            if (PlayersManager.GetPlayerByName(playerName) == null) {
                PlayerName.Value = playerName;
                TryConnectionResultClientRpc(true, NetcodeRpcUtil.GetClientRpcParams(OwnerClientId));
            } else {
                TryConnectionResultClientRpc(false, NetcodeRpcUtil.GetClientRpcParams(OwnerClientId));
            }
        }

        public override void OnNetworkDespawn() {
            PlayersManager.Unregister(OwnerClientId);
        }

        [ClientRpc]
        public void TryConnectionResultClientRpc(bool result, ClientRpcParams rpcParams = default) {
            onConnectionResult(result);
        }

        public void LoadGameScene() {
            SceneManager.LoadScene(1);
            RequestSpawnServerRpc(OwnerClientId);
        }
        

        [ServerRpc]
        public void RequestSpawnServerRpc(ulong ownerId) {
            SpawnPoint spawn = SpawnManager.Instance.GetFreeSpawn();
            PlayerInstance = Instantiate(playerPrefab, spawn.transform.position, Quaternion.identity);
            PlayerInstance.transform.LookAt(spawn.StartDirection);
            PlayerInstance.GetComponent<NetworkObject>().SpawnAsPlayerObject(ownerId);
            PlayerInstance.GetComponent<PlayerGraphicsController>().color.Value = Random.Range(0.0f, 1.0f);
            PlayerInstance.GetComponent<PlayerGraphicsController>().HandleCameraFacingClientRpc();
        }
    }
}