using Unity.Netcode;
using Unity.Netcode.Samples;
using UnityEngine;
using UnityEngine.Serialization;

namespace Player {
    [RequireComponent(typeof(Rigidbody), typeof(Collider))]
    public class NetworkPlayerPhysicsController : NetworkBehaviour {

        // Components
        private new Rigidbody rigidbody;
        private Transform playerTransform;
        private ClientNetworkTransform networkTransform;

        private Vector3 velocity;
        private Vector3 playerRotation;
        private float cameraRotation;
        private Vector3 currentCameraRotation;

        // View
        [FormerlySerializedAs("playerView")]
        [Header("View")]
        [SerializeField] private GameObject firstPersonView;
        [SerializeField] private GameObject head;
        
        
        // Settings
        [Header("Settings")]
        [SerializeField] private float speed;
        [SerializeField] private float mouseSensitivityX;
        [SerializeField] private float mouseSensitivityY;

        private void Awake() {
            rigidbody = GetComponent<Rigidbody>();
            playerTransform = GetComponent<Transform>();
            networkTransform = GetComponent<ClientNetworkTransform>();

            velocity = Vector3.zero;
            playerRotation = Vector3.zero;
            cameraRotation = 0.0f;
            currentCameraRotation = new Vector3(cameraRotation, 0.0f, 0.0f);
        }

        public override void OnNetworkSpawn() {
            if (!IsOwner) {
                enabled = false;
            }
        }

        private void FixedUpdate() {
            ProcessMovement();
            ProcessRotation();
        }

        private void ProcessMovement() {
            if (velocity != Vector3.zero) {
                Vector3 playerVelocity = playerTransform.forward * velocity.z + playerTransform.right * velocity.x;
                rigidbody.MovePosition(rigidbody.position + playerVelocity * Time.fixedDeltaTime);
            }
        }

        private void ProcessRotation() {
            rigidbody.MoveRotation(rigidbody.rotation * Quaternion.Euler(playerRotation));
            currentCameraRotation.x = Mathf.Clamp(currentCameraRotation.x - cameraRotation, -45.0f, 45.0f);
            firstPersonView.transform.localEulerAngles = currentCameraRotation; // Camera for 1st person
            head.transform.localEulerAngles = currentCameraRotation; // Camera for 3rd person
        }

        public void ResetPhysics() {
            rigidbody.constraints = RigidbodyConstraints.FreezeRotationX
                                    | RigidbodyConstraints.FreezeRotationY
                                    | RigidbodyConstraints.FreezeRotationZ;
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
            rigidbody.rotation = Quaternion.identity;
        }

        public void Explode(Vector3 direction) {
            rigidbody.constraints = RigidbodyConstraints.None;
            rigidbody.AddForce(direction * 25.0f, ForceMode.Impulse);
        }

        public void Teleport(Vector3 position, Vector3 direction) {
            playerRotation = Vector3.zero;
            cameraRotation = 0.0f;
            currentCameraRotation = new Vector3(cameraRotation, 0.0f, 0.0f);
            if (IsOwner) {
                networkTransform.Teleport(position, Quaternion.identity, Vector3.one);
                transform.LookAt(direction);
            }
        }

        public void SetDirection(Vector3 newDirection) {
            velocity = newDirection * speed;
        }

        public void SetRotation(Vector3 newRotation) {
            playerRotation = new Vector3(0.0f, newRotation.y * mouseSensitivityX, 0.0f);
            cameraRotation = newRotation.x * mouseSensitivityY;
        }
    }
}
