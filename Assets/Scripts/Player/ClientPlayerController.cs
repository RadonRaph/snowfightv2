using System;
using DG.Tweening;
using Player.Gameplay;
using UnityEngine;
using UnityEngine.InputSystem;
using Unity.Netcode;

namespace Player {
    [RequireComponent(typeof(NetworkPlayerPhysicsController),
        typeof(ServerSnowballLauncher),
        typeof(NetworkLifeState))]
    [DisallowMultipleComponent]
    public class ClientPlayerController : NetworkBehaviour {
        // Controllers
        private NetworkPlayerPhysicsController physicsController;

        // Network
        private ClientActionPerformer actionPerformer;
        private NetworkLifeState lifeState;

        // Events
        public static event Action<ClientPlayerController> onLocalClientSpawned = delegate { };
        public static event Action<ClientPlayerController> onLocalClientDespawned = delegate { };

        private bool IsAlive => lifeState.alive.Value;
        private bool canShoot;

        public void Awake() {
            physicsController = GetComponent<NetworkPlayerPhysicsController>();

            actionPerformer = GetComponent<ClientActionPerformer>();
            lifeState = GetComponent<NetworkLifeState>();

            canShoot = true;
        }

        public override void OnNetworkSpawn() {
            if (IsOwner) {
                onLocalClientSpawned(this);
            }
        }

        public override void OnNetworkDespawn() {
            onLocalClientDespawned(this);
        }

        public void OnMove(InputAction.CallbackContext ctx) {
            if (IsAlive && IsOwner) {
                Vector2 input = ctx.ReadValue<Vector2>();
                physicsController.SetDirection(new Vector3(input.x, 0.0f, input.y));
            }
        }

        public void OnRotate(InputAction.CallbackContext ctx) {
            if (IsAlive && IsOwner) {
                Vector2 input = ctx.ReadValue<Vector2>();
                physicsController.SetRotation(new Vector3(input.y, input.x, 0.0f));
            }
        }

        public void OnShoot(InputAction.CallbackContext ctx) {
            if (IsAlive && IsOwner && canShoot) {
                if (ctx.started) {
                    //canShoot = false;
                    DOVirtual.DelayedCall(3.0f, () => canShoot = true);
                    actionPerformer.PerformLaunchSnowball();
                }
            }
        }
    }
}