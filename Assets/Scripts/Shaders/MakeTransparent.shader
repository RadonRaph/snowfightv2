Shader "CustomRenderTexture/Simple"
{
    Properties
    {
       // _Color ("Color", Color) = (1,1,1,1)
        _Tex("InputTex", 2D) = "white" {}
     }

     SubShader
     {
        Lighting Off
        Blend One Zero

        Pass 
        {
            CGPROGRAM
            #include "UnityCustomRenderTexture.cginc"
            #pragma vertex CustomRenderTextureVertexShader
            #pragma fragment frag
            #pragma target 3.0
            

         //   float4      _Color;
            sampler2D   _Tex;

            float4 frag(v2f_customrendertexture IN) : COLOR
            {
                float4 texIn = tex2D(_Tex, IN.localTexcoord.xy);
                float a =1;

                if (max(max(texIn.x, texIn.y),texIn.z) < 0.5)
                {
                    a=0;
                }
                
                return float4(texIn.x, texIn.y, texIn.z, a);
            }
            ENDCG
            }
    }
}