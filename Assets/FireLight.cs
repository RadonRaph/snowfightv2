using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

public class FireLight : MonoBehaviour
{
    public Light light;
    
    // Start is called before the first frame update
    void Start()
    {
        light = GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        light.colorTemperature = Mathf.Lerp(light.colorTemperature, Random.Range(1500, 4000), 0.2f) ;
    }
}
