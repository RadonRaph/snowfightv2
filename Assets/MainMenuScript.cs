using Network;
using Player;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour {
    public Text errorText;

    public InputField nameInput;
    public Slider qualitySlider;
    public Slider volumeSlider;

    private string playerName;
    private bool valid = false;
    private PersistentPlayer player;

    private string[] banWords = new[] {
        "bite",
        "couille",
        "chatte",
        "merde",
        "nique",
        "pute",
        "bitch",
        "ass",
        "dick",
        "fuck",
        "cul",
        "salope",
        "suce",
        "homo",
    };

    public AudioMixer mixer;

    private void Start() {
        if (PlayerPrefs.HasKey("Volume")) {
            volumeSlider.value = PlayerPrefs.GetFloat("Volume");
            ChangeVolume();
        }

        if (PlayerPrefs.HasKey("Quality")) {
            qualitySlider.value = PlayerPrefs.GetInt("Quality");
        }

        playerName = "";
        player = null;
#if UNITY_SERVER

       StartServer();
       return;
#endif
        if (Application.isEditor) {
            StartServer();
            return;
        }
#if UNITY_CLIENT
        NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnected;
        if (!NetworkManager.Singleton.StartClient()) {
            errorText.text = "Connection to the game server failed";
        }
#endif
    }

    public void StartServer() {
        NetworkManager.Singleton.OnServerStarted += OnServerStarted;
        NetworkManager.Singleton.StartServer(); 
    }

    public void OnServerStarted() {
        NetworkManager.Singleton.OnServerStarted -= OnServerStarted;
        SceneManager.LoadScene(1);
    }
    
    public void OnClientConnected(ulong clientId) {
        NetworkManager.Singleton.OnClientConnectedCallback -= OnClientConnected;
        PersistentPlayer.onConnectionResult += OnConnectionResultReceived;
        player = PlayersManager.LocalPlayer;
    }

    public void ChangeQuality() {
        float v = qualitySlider.value;
        PlayerPrefs.SetInt("Quality", (int) v);
    }

    public void ChangeVolume() {
        float val = Remap(volumeSlider.value, 0, 1.2f, 0, 1f);
        mixer.SetFloat("MainVol", PercentToDb(val * 130));
        PlayerPrefs.SetFloat("Volume", volumeSlider.value);
    }

    private static float PercentToDb(float percent) {
        float db = -((percent * percent) / 375f) + ((16 * percent) / 15) - 80;
        return db;
    }

    private static float Remap(float value, float from1, float to1, float from2, float to2) {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    public void Play() {
        if (player != null) {
            if (playerName.Length > 0 && valid) {
                player.TryConnectServerRpc(playerName);
            } else {
                errorText.text = "Name must be specified";
            }
        } else {
            errorText.text = "You must be connected to the server to play";
        }
        
    }

    public void OnConnectionResultReceived(bool result) {
        if (result) {
            player.LoadGameScene();
        } else {
            errorText.text = "Name is already taken by one of the players in the game";
        }
    }

    public void Quit() {
        Application.Quit();
    }

    public void UpdatePlayerName() {
        string text = nameInput.text;

        valid = false;
        if (text.Length == 0) {
            errorText.text = "Name must be specified";
        } else if (text.Length > 15) {
            errorText.text = "Name must be less than 15 characters";
        } else if (ProfanityFilter(text)) {
            errorText.text = "Name must not be offensive";
        } else {
            errorText.text = "";
            playerName = text;
            valid = true;
        }
    }

    private bool ProfanityFilter(string text) {
        string lowerText = text.ToLower();
        foreach (string word in banWords) {
            if (lowerText.Contains(word)) {
                return true;
            }
        }
        return false;
    }
}